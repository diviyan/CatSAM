"""Testing catSAM on the asia dataset.

Author: Diviyan Kalainathan
"""

from catsam.catsam_onehot import run_SAM, SAM
import pandas as pd
import numpy as np
from sklearn.metrics import average_precision_score

a = pd.read_csv("alarm_categorical.csv")
# result = run_SAM(a, batch_size=1000, train_epochs=200, test_epochs=200,
#                  dnh=20, nh=5, lr=0.001, plot_generated_pair=False)
model = SAM(lr=0.005, dlr=0.005, l1=0.1, nh=5, dnh=50,
            train_epochs=10, test_epochs=10, batchsize=1000)

result = model.predict(a, nruns=4, gpus=1, njobs=2, plot_generated_pair=False)
# print(result)
# res = np.stack([np.sum(result[i*3:(i+1)*3, :], axis=0) for i in range(result.shape[1])])
pd.DataFrame(result, columns=a.columns).to_csv("pred_sam_alarm_onehot.csv", index=False)
# pd.DataFrame(res, columns=a.columns).to_csv("pred_sam_alarm.csv", index=False)
print(a.columns, pd.read_csv("alarm_target_adj.csv").columns)
score = average_precision_score(pd.read_csv("alarm_target_adj.csv").as_matrix().ravel(), result.ravel())
print(score)
