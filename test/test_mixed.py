"""Testing catSAM on the asia dataset.

Author: Diviyan Kalainathan
"""

from catsam.catsam import SAM
import pandas as pd
import numpy as np
from sklearn.metrics import average_precision_score

a = pd.read_csv("data/alarm_categorical.csv")
# result = run_SAM(a, batch_size=1000, train_epochs=200, test_epochs=200,
#                  dnh=20, nh=5, lr=0.001, plot_generated_pair=False)
model = SAM(lr=0.01, dlr=0.01, l1=0.1, nh=5, dnh=50,
            train_epochs=20, test_epochs=20, batchsize=1000)


result = model.predict(a, [True for i in a.columns],
                       nruns=1, gpus=0, njobs=1, plot_generated_pair=False)
# print(result)
# res = np.stack([np.sum(result[i*3:(i+1)*3, :], axis=0) for i in range(result.shape[1])])

pd.DataFrame(result, columns=a.columns).to_csv("pred_sam_alarm_o_re.csv", index=False)
# pd.DataFrame(res, columns=a.columns).to_csv("pred_sam_alarm.csv", index=False)
# print(a.columns, pd.read_csv("abalone_target.csv").columns)
score = average_precision_score(pd.read_csv("data/alarm_target_adj.csv").as_matrix().ravel(), result.ravel())
#
# result = model.predict(a, [True, False, False, False, False, False, False, False, True],
#                        nruns=1, gpus=1, njobs=1, plot_generated_pair=False)
# # print(result)
# # res = np.stack([np.sum(result[i*3:(i+1)*3, :], axis=0) for i in range(result.shape[1])])
#
#
# pd.DataFrame(result, columns=a.columns).to_csv("pred_sam_abalone.csv", index=False)
# # pd.DataFrame(res, columns=a.columns).to_csv("pred_sam_alarm.csv", index=False)
# print(a.columns, pd.read_csv("abalone_target.csv").columns)
# score = average_precision_score(pd.read_csv("abalone_target.csv").as_matrix().ravel(), result.ravel())
#

print(result)
print(score)
