"""Testing catSAM on the asia dataset.

Author: Diviyan Kalainathan
"""

from catsam.catsam_binary import run_SAM, SAM
import pandas as pd
import numpy as np
from sklearn.metrics import average_precision_score

a = pd.read_csv("asia_data.csv")
# result = run_SAM(a, batch_size=1000, train_epochs=200, test_epochs=200,
#                  dnh=20, nh=5, lr=0.001, plot_generated_pair=False)
model = SAM(lr=0.1, dlr=0.1, l1=0.1, nh=5, dnh=20,
            train_epochs=50, test_epochs=50, batchsize=1000)

result = model.predict(a, nruns=20, gpus=1, njobs=3)
# print(result)
# res = np.stack([np.sum(result[i*3:(i+1)*3, :], axis=0) for i in range(result.shape[1])])
pd.DataFrame(result, columns=a.columns).to_csv("pred_sam_asia.csv", index=False)
# pd.DataFrame(res, columns=a.columns).to_csv("pred_sam_alarm.csv", index=False)
score = average_precision_score(pd.read_csv("asia_target.csv").as_matrix().ravel(), result.ravel())
print(score)
